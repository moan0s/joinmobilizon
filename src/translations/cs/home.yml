devby: vyvinul @:color.soft
intro:
  tags:
  - Shromažďujte se
  - Mobilizujte
  support: Podpořte nás
  title: Uživatelsky přívětivý, emancipační a etický nástroj pro shromažďování, organizování
    a mobilizaci.
prez:
  tool:
    title: Praktický nástroj
    md: "Mobilizon je nástroj, který umožňuje **vyhledávat, vytvářet a organizovat\n\
      události**.\n\nMůžete také **zveřejnit stránku své skupiny**, kde se\nčlenové\
      \ mohou **společně organizovat**."
  alt:
    title: Etická alternativa
    md: "Etická alternativa k událostem, skupinám a stránkám na Facebooku, Mobilizon\n\
      je **nástroj určený k tomu, aby vám sloužil**. Tečka.\n\nŽádný _like_, žádný\
      \ _follow_, žádná zeď s nekonečným _scrollováním_: **Mobilizon\nponechává vám\
      \ kontrolu nad vaší pozorností**."
  soft:
    title: Federativní software
    md: "Mobilizon není obří platforma, ale \n**mnoho vzájemně propojených webových\
      \ stránek Mobilizon**.\n\nTato federativní architektura umožňuje **vyhnout se\
      \ monopolům** \na nabídnout **rozmanitost podmínek hostování**."
  btn:
  - Co je Mobilizon?
  - Více informací najdete na webu mobilizon.org
what:
  intro: "## Co je Mobilizon?\n\nMobilizon je online nástroj, který vám pomůže \n\
    spravovat události, profily a skupiny."
  event:
    title: Vaše události
    md: "V aplikaci Mobilizon můžete **vytvořit podrobný záznam události**, \nzveřejnit\
      \ jej a sdílet.\n\nMůžete také vyhledávat události podle klíčových slov, místa\
      \ nebo data \na **registrovat se na ně (aniž byste nutně potřebovali účet)**\
      \ \na přidat je do kalendáře."
  group:
    title: Vaše skupiny
    md: "V Mobilizon má **každá skupina veřejnou stránku**, \nkde si můžete prohlédnout\
      \ nejnovější **příspěvky a veřejné události** skupiny.\n\nČlenové pozvaní do\
      \ skupiny se mohou **účastnit diskusí \na spravovat společnou složku zdrojů**\
      \ \n(odkaz na nástroj pro společné psaní, wiki atd.)"
  profile:
    title: Vaše profily
    md: "Vytvoření účtu na fóru Mobilizon vám umožní \n**vytvořit si více profilů**\
      \ (osobní, profesní, zájmový, aktivistický atd.), \n**organizovat události**\
      \ a **spravovat skupiny**.\n\nNezapomeňte si před vytvořením účtu \n**zjistit,\
      \ jak daná instance funguje, jak je popsáno na stránce \"o ní \"**, \na porozumět\
      \ jejím pravidlům a zásadám."
design:
  attention:
    title: Úspora vaší pozornosti
    md: "Mobilizon není **médium ani koníček: je to nástroj**. \nNenajdete zde funkce\
      \ jako počítadlo odběrů, palce nahoru u komentářů \nnebo nekonečné <i lang=\"\
      cs\">procházení</i> \ninformačními příspěvky.\n\nCílem designu Mobilizonu je\
      \ **uvolnit \nvaši pozornost od samočinných mechanismů**.\nDíky tomu se můžete\
      \ soustředit na to nejdůležitější: \nsprávu událostí, skupin a mobilizaci."
  federation:
    title: Rozmanitost prostřednictvím federace
    md: "Mobilizon je **federovaný software**: hostitelé jej mohou nainstalovat \n\
      na server a vytvořit co nejvíce instancí, webových stránek Mobilizon.\nInstance\
      \ služby Mobilizon lze sdružovat, takže profil registrovaný \nv instanci A může\
      \ přispívat do skupiny vytvořené v instanci B.\n\nTato mnohost umožňuje **diverzifikovat\
      \ podmínky hostování** \n(správa, CGU, charty) a **vyhnout se vzniku \nmonopolních\
      \ platforem**."
  freedom:
    title: Respektování vašich svobod
    md: "Mobilizon je software s otevřeným zdrojovým kódem. \nTo znamená, že jeho\
      \ kód je transparentní, veřejně prohledatelný a že **neexistuje žádná skrytá\
      \ funkce**.\n\nKód je **komunitně vytvořen** a každý jej může \npřevzít zpět\
      \ a pokusit se projekt vést novým směrem."
  intro: "## Emancipační nástroj, navržený jinak\n\nMobilizon byl navržen jako praktický\
    \ nástroj, \nkterý respektuje vaši pozornost, autonomii a svobodu."
  go: "## Teď je řada na vás\n\n- Otestujte [Mobilizon demo](@:link.mzo-demo)\n- Najděte\
    \ svou [instanci na Mobilizon.org](@:link.mzo)\n- Jak používat (a instalovat)\
    \ Mobilizon se dozvíte na [naší dokumentaci](@:link.jmz-docs)\n- Přijďte diskutovat\
    \ na [naše fórum](@:link.jmz-forum)"
who:
  title: Kdo stojí za Mobilizonem?
  md: "Mobilizon je **svobodný open source software financovaný \nfrancouzským neziskovým\
    \ sdružením**: Framasoft.\n\nSdružení bylo založeno v roce 2004 a nyní se věnuje\
    \ **populárnímu vzdělávání \nv oblasti digitálních technologií**. Naše malá struktura\
    \ (méně než 40 členů, \nméně než 10 zaměstnanců) je známá tím, že realizovala\
    \ internetový projekt \nDégooglisons, který navrhl 34 etických a alternativních\
    \ online nástrojů.\nNaše sdružení, které je uznáváno jako obecně prospěšná organizace,\
    \ **je z více \nnež 90 % financováno z vašich darů**, které si francouzští daňoví\
    \ poplatníci mohou odečíst z daní.\n\n**Čím více bude tento software používán\
    \ a podporován, tím více lidí jej bude \npoužívat a přispívat do něj a tím rychleji\
    \ se z něj stane konkrétní alternativa \nk platformám, jako jsou Facebook Events\
    \ nebo Meetup**."
quote:
  text: Facebookem svět nezměníme. Nástroj, o kterém sníme, nejsou společnosti sledovacího
    kapitalismu schopny vyrobit, protože by nevěděly, jak na něm vydělat. <br /> Je
    to příležitost dělat věci jinak a lépe než oni.
  blog:
    text: Přečtěte si prohlášení společnosti Framasoft o záměru na Framablogu
title: Převezměme zpět kontrolu nad našimi událostmi
subtitle: Společně se dostaneme dál
